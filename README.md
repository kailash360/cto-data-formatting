# Script to upload CTO file

The [Upload.js](./Upload/Upload) file contains the code to upload the files to the IPFS. The modela nd template and template to be uploaded are present in the ```assets``` folder.  

## How to sue the script? 

The ```Uplaod.js``` file exports an asynchronous ```upload``` method that is responsible to upload the files to the IPFS. It returns an array of the upload results for the model and template, individually.