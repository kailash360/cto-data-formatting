/**
 * @brief Code to upload a new file on ipfs
 * @description 
 * This code first creates a random node on the IPFS 
 * and then uploads the files to that node. It then returns the IPFS hash
 * of the uploaded files.
 * @parameters - None
 * @returns IPFS hash of uploaded file
 */

const fs = require('fs')
const IPFS = require('ipfs-core')

const upload = async() => {

    try {
        //Create a node on the IPFS
        const node = await IPFS.create({ repo: 'ok' + Math.random() })

        //Upload the template and return the IPFS hash
        const modelUploadResult = await node.add("../assets/model.cto")
        const templateUploadResult = await node.add("../assets/templates.json")
        return [modelUploadResult, templateUploadResult]

    } catch (err) {
        console.log("Error in uploading file...", err)
        return err.message
    }
}

module.exports = upload